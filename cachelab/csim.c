/* 
 * Louis Raison    louis.raison
*/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include "cachelab.h"

/* Globals set by command line args */
int verbosity = 0; /* print trace if set */
int s = 0;		   /* set index bits */
int b = 0;		   /* block offset bits */
int E = 0;		   /* associativity */
char *trace_file = NULL;

/*
 * printUsage - Print usage info
 */
void printUsage(char *argv[])
{
	printf("Usage: %s [-hv] -s <num> -E <num> -b <num> -t <file>\n", argv[0]);
	printf("Options:\n");
	printf("  -h         Print this help message.\n");
	printf("  -v         Optional verbose flag.\n");
	printf("  -s <num>   Number of set index bits.\n");
	printf("  -E <num>   Number of lines per set.\n");
	printf("  -b <num>   Number of block offset bits.\n");
	printf("  -t <file>  Trace file.\n");
	printf("\nExamples:\n");
	printf("  linux>  %s -s 4 -E 1 -b 4 -t traces/yi.trace\n", argv[0]);
	printf("  linux>  %s -v -s 8 -E 2 -b 4 -t traces/yi.trace\n", argv[0]);
	exit(0);
}

/*
 * main - Main routine 
 */
int main(int argc, char *argv[])
{
	char c;

	while ((c = getopt(argc, argv, "s:E:b:t:vh")) != -1)
	{
		switch (c)
		{
		case 's':
			s = atoi(optarg);
			break;
		case 'E':
			E = atoi(optarg);
			break;
		case 'b':
			b = atoi(optarg);
			break;
		case 't':
			trace_file = optarg;
			break;
		case 'v':
			verbosity = 1;
			break;
		case 'h':
			printUsage(argv);
			exit(0);
		default:
			printUsage(argv);
			exit(1);
		}
	}

	/* Make uintptr_t sure that all required command line args were specified */
	if (s == 0 || E == 0 || b == 0 || trace_file == NULL)
	{
		printf("%s: Missing required command line argument\n", argv[0]);
		printUsage(argv);
		exit(1);
	}

	/* Implementation of a cache.
	 * Here we store the tags in the cache array, allocated with malloc. No real data is stored in this case.
	 * We use the least-recent-update method to update our cache.
	 */

	//Init Variables

	int S = 1 << s;
	int hit = 0;
	int miss = 0;
	int evic = 0;
	// The cache actually stores tags, not really data here
	long long *cache = malloc(S * E * sizeof(long long));
	// least_recent has the same structure than the cache and contains
	// the last time elements were modified
	unsigned least_recent[S][E];

	long long tagmask = (1 << (64 - s - b)) - 1;

	/* Init cache to -1 for all and least_recent matrix
	 * -1 also ensures that the bits are not valid bacause the size
	 * of the tags is limited to 62 bits.
	 * Thus, -1 cannot be attained by any tag configuration.
	 */
	for (int i = 0; i < S; i++)
	{
		for (int j = 0; j < E; j++)
		{
			cache[(i * E) + j] = -1;
			least_recent[i][j] = 0;
		}
	}

	// Read input file
	FILE *handle = fopen(trace_file, "r");
	if (handle == NULL)
	{
		fprintf(stderr, "Cannot open file.\n");
		exit(1);
	}

	char buffer[128];

	unsigned time_stamp = 0;

	// Read each line
	while (fgets(buffer, 128, handle) != NULL)
	{
		if (buffer[0] != ' ')
			continue;
		time_stamp++;

		// Read info from the line
		char action;
		uintptr_t addr;
		int offset;
		sscanf(buffer + 1, "%c%lx,%d", &action, &addr, &offset);

		long long tag;
		long long set_index;
		long long block_offset; // Unused here

		// Clean addr
		set_index = (addr &((1 << (s + b)) - 1))>>b; // Keep only the adress bits
		block_offset = addr & ~(~0 << b);

		if (verbosity)
		{
			printf("Action : %c, \t Adress : %lx, \t Offset : %d\n", action, addr, offset);
			printf("Set : %llx, Block Offset : %llx", set_index, block_offset);
		}
		
		// Tag
		tag = (addr >> (s + b)) & tagmask;
		int found = 0;

		// min_time finds the oldest element (to evict)
		unsigned min_time = 1 << 31;
		int c_min_time;

		// Handle different cases
		switch (action)
		{
		case 'L':
			// Load

			// Find Tag
			for (int j = 0; j < E; j++)
			{
				long long cache_elem = cache[set_index * E + j];
				if (least_recent[set_index][j] < min_time)
				{
					min_time = least_recent[set_index][j];
					c_min_time = j;
				}
				if (tag == cache_elem)
				// Tags correspond AND is valid
				{
					++hit;
					++found;
					least_recent[set_index][j] = time_stamp; // Update time
					break;
				}
			}
			if (!found)
			{
				++miss;
				// Evict the oldest element and replace it.
				cache[(set_index * E) + c_min_time] = tag;
				least_recent[set_index][c_min_time]= time_stamp;
				if (min_time != 0) // The cache has already been filled before
				{
					++evic;
				}
			}
			break;
		case 'S':
			// Store (Same as Load)
			for (int j = 0; j < E; j++)
			{
				long long cache_elem = cache[set_index * E + j];
				if (least_recent[set_index][j] < min_time)
				{
					min_time = least_recent[set_index][j];
					c_min_time = j;
				}
				if (tag == cache_elem)
				{
					++hit;
					++found;
					least_recent[set_index][j] = time_stamp;
					break;
				}
			}
			if (!found)
			{
				++miss;
				cache[(set_index * E) + c_min_time] = tag;
				least_recent[set_index][c_min_time]= time_stamp;
				if (min_time != 0)
				{
					++evic;
				}
			}
			break;
		case 'M':
			// Modify = Load and Store
			for (int j = 0; j < E; j++)
			{
				long long cache_elem = cache[set_index * E + j];
				if (least_recent[set_index][j] < min_time)
				{
					min_time = least_recent[set_index][j];
					c_min_time = j;
				}
				if (tag == cache_elem)
				{
					hit = hit+2;
					++found;
					least_recent[set_index][j] = time_stamp;
					break;
				}
			}
			if (!found)
			{
				++miss;
				++hit; // The element will be loaded in the cache afterwards
				cache[(set_index * E) + c_min_time] = tag;
				least_recent[set_index][c_min_time]= time_stamp;
				if (min_time != 0)
				{
					++evic;
				}
			}
			break;
		default:
			printf("%c character not recognized. Use only L, M and S.\n", action);
			break;
		}
	}

	fclose(handle);

	printSummary(hit, miss, evic);
	return 0;
}
