/* 
 * trans.c - Matrix transpose B = A^T
 *
 * Each transpose function must have a prototype of the form:
 * void trans(int M, int N, int A[N][M], int B[M][N]);
 *
 * A transpose function is evaluated by counting the number of misses
 * on a 1KB direct mapped cache with a block size of 32 bytes.
 */
#include <stdio.h>
#include "cachelab.h"

int is_transpose(int M, int N, int A[N][M], int B[M][N]);

/* 
 * trans - A block by block version of the algorithm.
 */
char block_trans_desc[] = "Block by block Transpose.";
void block_trans(int M, int N, int A[N][M], int B[M][N], int block_size)
{
    int i, j, k, l, tmp, d;
    // The block is between the i*block_size row and the (i+1)*block_size line and same for columns.
    // It is a square submatrix of the initial matrix.
    for (i = 0; i < N; i += block_size)
    {
        for (j = 0; j < M; j += block_size)
        {
            for (k = i; k < i + block_size; k++)
            {
                for (l = j; l < j + block_size; l++)
                {
                    // Special case for the diagonal elements : risk of evicting each other
                    // We deal with them later
                    if (k != l)
                    {
                        B[l][k] = A[k][l];
                    }
                    else
                    {
                        tmp = A[k][l];
                        d = l;
                    }
                }
                if (i == j)
                {
                    B[d][d] = tmp;
                }
            }
        }
    }
}

/* 
 * transpose_submit - This is the solution transpose function that you
 *     will be graded on for Part B of the assignment. Do not change
 *     the description string "Transpose submission", as the driver
 *     searches for that string to identify the transpose function to
 *     be graded. 
 */
char transpose_submit_desc[] = "Transpose submission";
void transpose_submit(int M, int N, int A[N][M], int B[M][N])
{
    if (M == 32)
    {
        block_trans(M, N, A, B, 8);
    }
    else if (N == 64)
    {
        block_trans(M, N, A, B, 4);
    }
    else
    {
        // For the last case. Exactly the same method but with a different block_size
        // + an additional check because the matrix dimensions are not perfectly fixing the blocks.
        int i, j, k, l, tmp, d;
        int block_size = 16;
        for (i = 0; i < N; i += block_size)
        {
            for (j = 0; j < M; j += block_size)
            {
                // We check wether the couple (k,l) is in the correct indices of the array
                for (k = i; (k < i + block_size) && (k < N); k++)
                {
                    for (l = j; (l < j + block_size) && (l < M); l++)
                    {
                        if (k != l)
                        {
                            B[l][k] = A[k][l];
                        }
                        else
                        {
                            tmp = A[k][l];
                            d = l;
                        }
                    }
                    if (i == j)
                    {
                        B[d][d] = tmp;
                    }
                }
            }
        }
    }
}

/* 
 * You can define additional transpose functions below. We've defined
 * a simple one below to help you get started. 
 */

/* 
 * trans - A simple baseline transpose function, not optimized for the cache.
 */
char trans_desc[] = "Simple row-wise scan transpose";
void trans(int M, int N, int A[N][M], int B[M][N])
{
    int i, j, tmp;

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
        {
            tmp = A[i][j];
            B[j][i] = tmp;
        }
    }
}

/*
 * registerFunctions - This function registers your transpose
 *     functions with the driver.  At runtime, the driver will
 *     evaluate each of the registered functions and summarize their
 *     performance. This is a handy way to experiment with different
 *     transpose strategies.
 */
void registerFunctions()
{
    /* Register your solution function */
    registerTransFunction(transpose_submit, transpose_submit_desc);

    /* Register any additional transpose functions */
    // registerTransFunction(trans, trans_desc);
    // registerTransFunction(block_trans, block_trans_desc);
}

/* 
 * is_transpose - This helper function checks if B is the transpose of
 *     A. You can check the correctness of your transpose by calling
 *     it before returning from the transpose function.
 */
int is_transpose(int M, int N, int A[N][M], int B[M][N])
{
    int i, j;

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; ++j)
        {
            if (A[i][j] != B[j][i])
            {
                return 0;
            }
        }
    }
    return 1;
}
